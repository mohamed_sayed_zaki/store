<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductsTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testProduct()
    {
		
        $this->browse(function ($browser){
            $browser->visit('/products')
			->assertSee('Show All Products');
        });		
    }
}
