<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProdcutsController extends Controller
{
	public function getAllProducts(){
	    $products = Product::get();
	    return ['data'=>$products];
	}

	public function searchProducts(Request $request){
	    $products = Product::where('name','like', '%' . $request->keywords . '%')->orWhere('category','like', '%' . $request->keywords . '%')->orWhere('price','like', '%' . $request->keywords . '%')->get();
	    return ['data'=>$products];		
	}
}
